package com.qaagility.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ControllerTest {

	@Test
	public void testd1() throws Exception {

		int k = new Controller().division(15, 3);
		assertEquals("Add", 5, k);

	}

	@Test
	public void testd2() throws Exception {

		int k = new Controller().division(3, 0);
		assertEquals("Add", Integer.MAX_VALUE, k);

	}
}


